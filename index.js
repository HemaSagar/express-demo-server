const express = require('express')
const app = express();
const port = process.env.PORT || 4000;

app.get('/',(req,res)=> {
    res.status(200).send("request response ok");
})

app.get('/user:id',(req,res)=>{
    res.send(`response for user ${req.params.id}`);
})

app.get('/download',(req,res)=>{
    res.download('./index.js');
})

app.listen(port,()=> {
    console.log("listening to port:",port);
});